# -*- coding: utf-8 -*-

# from odoo import models, fields, api


# class pokemon(models.Model):
#     _name = 'pokemon.pokemon'
#     _description = 'pokemon.pokemon'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

from odoo import models, fields, api

class entrenador(models.Model):
    _name = 'pokemon.entrenador'
    _description = 'Permite definir las caracteristicas de un equipo'
    _order = 'name'
    
    name = fields.Char(string='Nombre', required=True, size=10)
    fechanacimiento = fields.Date(string='Fecha de nacimiento')
    sexo = fields.Selection(string='Sexo', selection=[('h','hombre'), ('m','mujer'), ('o','otro')], default='o')
    annos = fields.Integer('Años', compute='_get_annos')
    
    @api.depends('fechanacimiento')
    def _get_annos(self):
        for entrenador in self:
            entrenador.annos = 0
    
    #relaciones
    equipo_ids = fields.One2many('pokemon.equipo','entrenador_id',string='Equipos')

class equipo(models.Model):
    _name = 'pokemon.equipo'
    _description = 'Permite definir las caracteristicas de un equipo'
    
    name = fields.Char(string='Codigo', required=True, size=6)
    modalidad = fields.Char(string='Modalidad')
    
    #relaciones
    pokemons_ids = fields.One2many('pokemon.pokemons','equipo_id',string='Pokemons')
    entrenador_id = fields.Many2one('pokemon.entrenador', string='Entrenador')
    
class pokemons(models.Model):
    _name = 'pokemon.pokemons'
    _description = 'Permite definir las caracteristicas de un pokemon'
    _order = 'name'
    
    name = fields.Char(string='Nombre', required=True)
    tipoprimario = fields.Char(string='Tipo Primario', required=True)
    tiposecundario = fields.Char(string='Tipo Secundario')
    habilidad = fields.Char(string='Habilidad', required=True)
    
    #relaciones
    rutas_ids = fields.Many2many('pokemon.rutas',string='Rutas')
    equipo_id = fields.Many2one('pokemon.equipo', string='Equipo')
    
class rutas(models.Model):
    _name = 'pokemon.rutas'
    _description = 'Permite definir las caracteristicas de una ruta'
    _order = 'name'
    
    name = fields.Char(string='Nombre', required=True)
    region = fields.Char(string='Region')
    nivelmin = fields.Integer(string='Nivel Mínimo')
    nivelmax = fields.Integer(string='Nivel Máximo')
    
    #relaciones
    pokemons_ids = fields.Many2many('pokemon.pokemons',string='Pokemons')