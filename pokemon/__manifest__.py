# -*- coding: utf-8 -*-
{
    'name': "Gestión Pokemon",

    'summary': """
        Gestión de entrenadores y sus respectivos equipos Pokemon.""",

    'description': """
        Esto es un modulo basado en el videojuego Pokemon donde se puede gestionar unos entrenadores y sus respectivos equipos formados por Pokemon y donde se encuentran estos.
    """,

    'author': "Gisela",
    'website': "https://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    #Indicamos que es una aplicación
    'application': True,
}
